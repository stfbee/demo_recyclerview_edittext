package ru.vladonishchenko.demo_recyclerview_edittext;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rec_view);
		recyclerView.setLayoutManager(new LinearLayoutManager(this));
		Adapter adapter = new Adapter();
		recyclerView.setAdapter(adapter);
	}

	static class Adapter extends RecyclerView.Adapter<Adapter.Holder> {
		List<Item> items = new ArrayList<>();

		Adapter() {
			this.items = Item.getItems();
		}

		@Override
		public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
			Context context = parent.getContext();
			LayoutInflater inflater = LayoutInflater.from(context);
			View view = inflater.inflate(R.layout.list_item_et, parent, false);
			return new Holder(view);
		}

		@Override
		public void onBindViewHolder(Holder holder, int position) {
			Item item = items.get(position);
			holder.text.setText(String.format(Locale.getDefault(), "Item #%d", item.index));
			holder.edit_text.setVisibility(item.editable ? View.VISIBLE : View.GONE);
		}

		@Override
		public int getItemCount() {
			return items.size();
		}

		static class Holder extends RecyclerView.ViewHolder {
			EditText edit_text;
			TextView text;

			Holder(View itemView) {
				super(itemView);

				edit_text = (EditText) itemView.findViewById(R.id.edit_text);
				text = (TextView) itemView.findViewById(R.id.text);
			}
		}
	}
}
