package ru.vladonishchenko.demo_recyclerview_edittext;

import java.util.ArrayList;
import java.util.List;


public class Item {
	int index;
	boolean editable;

	public Item(int index, boolean editable) {
		this.index = index;
		this.editable = editable;
	}

	public static List<Item> getItems() {
		ArrayList<Item> _list = new ArrayList<>();
		for (int i = 0; i < 100; i++) {
			_list.add(new Item(i + 1, i % 3 == 2));
		}
		return _list;
	}
}
